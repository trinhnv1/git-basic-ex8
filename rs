Bước 1: vào link http://git@gitlab.com/tubcvt/git-basic-ex8.git
Bước 2: 
    - Chọn fork -> chọn repo của mình
    - git clone https://gitlab.com/trinhnv1/git-basic-ex8.git
Bước 3: Kéo code
    - Di chuyển đến thư mục git-basic-ex8 bằng lệnh cd git-basic-ex8
    - pull code : git pull https://gitlab.com/trinhnv1/git-basic-ex8.git
Bước 4: kết nối với project đã fork
    - git remote add upstream http://git@gitlab.com/tubcvt/git-basic-ex8.git
Bước 5:
    - Check toàn bộ nhánh có trong project: git branch -a
    - Checkout qua nhánh f1: git checkout f1
    - Check log để xem commit : git log
    - reset commit : git reset --hard 138fbca72c8a79fe7d9d941d39b19e900431b99c
    - reset về commit cũ chỉ thực hiện trên local khi chưa push code lên git
